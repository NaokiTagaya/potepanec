require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:taxonomy) { create(:taxonomy, name: "Category") }
    let(:tshirt) { create(:taxon, name: "T-Shirt", taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:rails_tshirt) { create(:product, taxons: [tshirt], name: "Ruby on Rails Ringer T-Shirt") }

    before do
      get potepan_category_url(tshirt.id)
    end

    it "returns http success" do
      expect(response).to be_successful
      expect(response).to have_http_status(200)
    end

    it "get data success" do
      expect(assigns(:taxonomies)).to match_array taxonomy
      expect(assigns(:taxon)).to eq tshirt
      expect(assigns(:products)).to match_array rails_tshirt
    end
  end
end
