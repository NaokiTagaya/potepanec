require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /show" do
    subject(:related_products_limit) do
      product.related_products.includes(master: [:default_price, :images]).limit(4)
    end

    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, name: "Taxon", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:product) { create(:product, taxons: [taxon], name: "Product", price: "19.99") }
    let(:image) { create(:image, viewable_id: product.id) }
    let!(:related_products) do
      create_list(:product, 4, price: "#{rand(1.0..99.9).round(2)}", taxons: [taxon])
    end

    before do
      get potepan_product_url(product.id)
    end

    context "product information page" do
      it "returns http success" do
        expect(response).to have_http_status(200)
      end

      it "show product information" do
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price.to_s
        expect(response.body).to include product.description
      end

      it "get data success" do
        expect(assigns(:product)).to eq product
      end

      it "get product image" do
        product.images.each do |image|
          expect(response.body).to include image.attachment(:product)
          expect(response.body).to include image.attachment(:small)
        end
      end

      it "get related product infomation" do
        expect(assigns(:related_products)).to match_array related_products
      end
    end

    context "There are 5 related products" do
      let!(:related_products) do
        create_list(:product, 5, taxons: [taxon])
      end

      it "If you have 5 related products you get 4 products" do
        expect(assigns(:related_products).size).to eq 4
      end
    end
  end
end
