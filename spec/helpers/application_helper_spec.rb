require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'page_title' do
    context 'page_title as test' do
      it 'display full title' do
        expect(page_title('test')).to eq 'test - BIGBAG Store'
      end
    end

    context 'page_title is blank' do
      it 'display only BIGBAG Store' do
        expect(page_title('')).to eq 'BIGBAG Store'
      end
    end

    context 'page_title is nil' do
      it 'display only BIGBAG Store' do
        expect(page_title(nil)).to eq 'BIGBAG Store'
      end
    end
  end
end
