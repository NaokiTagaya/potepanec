require 'rails_helper'

RSpec.describe "Product_model", type: :model do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Taxon", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:unrelated_product) { create(:product, name: "unrelated_product") }
  let(:products) { create_list(:product, 4, taxons: [product.taxons.first]) }

  describe "related_products" do
    context "Display related products" do
      it "Get the same taxon product" do
        expect(product.related_products).to eq products
      end

      it "Not get unrelated products" do
        expect(unrelated_product.related_products).to eq []
      end
    end
  end
end
