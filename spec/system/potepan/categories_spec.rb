require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon)    { create(:taxon, name: "Sneakers", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:product)  { create(:product, name: "RubySneaker", price: 20, taxons: [taxon]) }
  let(:image)    { create(:image) }

  before do
    product.master.images = [image]
    visit potepan_category_path(taxon.id)
  end

  describe "display category page" do
    it "The data associated with the category displayed" do
      expect(page).to have_selector ".page-title", text: "Sneakers"
      expect(page).to have_selector ".breadcrumb", text: "Sneakers"
      expect(page).to have_selector ".productCaption", text: "RubySneaker"
      expect(page).to have_selector ".productCaption", text: "20"
    end
  end

  describe "display side menu" do
    it "The number of products can be displayed correctly" do
      expect(page).to have_selector('.productBox', count: taxon.all_products.count)
    end

    it "Category links are implemented correctly" do
      within ".side-nav" do
        click_link "Sneakers"
      end
      expect(page).to have_selector ".page-title", text: "Sneakers"
      expect(page).to have_selector ".breadcrumb", text: "Sneakers"
      expect(page).to have_selector ".productCaption", text: "RubySneaker"
      expect(page).to have_selector ".productCaption", text: "20"
    end
  end

  describe "display product image" do
    it "The product image is displayed correctly" do
      expect(page).to have_selector("img[alt=#{product.name}]")
    end
  end
end
