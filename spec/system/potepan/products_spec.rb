require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon)    { create(:taxon, name: "Sneakers", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:product)  { create(:product, name: "RailsSneaker", price: 22.22, taxons: [taxon]) }
  let(:image)    { create(:image) }
  let!(:related_products) do
    4.times.map do |i|
      create(:product, name: "related_product#{i + 1}",
                       price: "#{rand(1.0..99.9).round(2)}",
                       taxons: [taxon])
    end
  end

  before do
    product.master.images = [image]
    visit potepan_product_path(product.id)
  end

  describe "display product detail page" do
    it "The product details screen is displayed normally" do
      expect(page).to have_title "#{product.name} - BIGBAG Store"
      expect(page).to have_selector ".page-title h2", text: product.name
      expect(page).to have_selector ".col-xs-6 li", text: product.name
      expect(page).to have_selector ".media-body h2", text: product.name
      expect(page).to have_selector ".media-body h3", text: product.display_price
      expect(page).to have_selector ".media-body p", text: product.description
    end

    it "Related products are displayed correctly" do
      related_products.each do |related_product|
        expect(page).to have_selector ".productCaption h5", text: related_product.name
        expect(page).to have_selector ".productCaption h3", text: related_product.display_price
        expect(page).to have_selector("img[alt=#{related_product.name}-img]")
      end
    end

    it "The screen can be transitioned normally from the link" do
      expect(page).to have_link 'Home', href: potepan_index_path
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
    end

    it "Transition by clicking related products" do
      related_products.each do |related_product|
        expect(page).to have_link "#{related_product.name}", href: potepan_product_path(related_product.id)
        click_on "#{related_product.name}"
      end
    end
  end
end
