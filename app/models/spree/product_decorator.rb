module Spree::ProductDecorator
  def related_products
    return Spree::Product.none if taxons.none?
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end
  Spree::Product.prepend self
end
